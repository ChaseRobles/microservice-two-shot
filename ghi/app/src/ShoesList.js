import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';





function ShoesColumn(props){

  const shoeDelete = (href) => async () => {
    const response = await fetch(`http://localhost:8080${href}`,{method: "delete"})
    if (response.ok){
      await props.getShoes()
    }

   }



  return (
    <div className="col">
      {props.list.map(shoe => {
        return (
          <div key={shoe.href} className="card mb-3 shadow">
            <img className="card-img-top" src={shoe.picture_url} alt="Card image cap" />
            <div className="card-body">
              <h5 className="card-title">{shoe.name}</h5>
              <p className="card-text">Manufacturer : {shoe.manufacturer}</p>
              <p className="card-text">Color : {shoe.color}</p>
            </div>
            <ul className="list-group list-group-flush">
              <li className="list-group-item">Bin Name: {shoe.bin.name}</li>
              <li className="list-group-item">Bin Number: {shoe.bin.bin_number}</li>
              <li className="list-group-item">Bin Size: {shoe.bin.bin_size}</li>
            </ul>
            <div className="card-body">
              {/* <a href="#" className="card-link">Card link</a>
              <a href="#" className="card-link">Another link</a> */}
                  <button onClick={ shoeDelete(shoe.href)
                  }>Delete</button>
            </div>
          </div>
          );
      })}
    </div>
  );
}

const ShoesList = (props) => {
  const [shoesColumns, setShoesColumns] = useState([[],[],[]]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/'
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const requests = [];
        for(let shoe of data.shoes){
          const detailUrl = `http://localhost:8080${shoe.href}`;
          requests.push(fetch(detailUrl));
        }
        const responses = await Promise.all(requests);

        const columns = [[],[],[]]

        let i = 0;
        for (const shoeResponse of responses) {
          if (shoeResponse.ok) {
            const details = await shoeResponse.json();
            columns[i].push(details);
            i += 1;
            if (i > 2) {
              i = 0
            }
          } else {
            console.error(shoeResponse);
          }
        }
        setShoesColumns(columns);


      }
    }
    catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {

    fetchData();
  }, []);


    return (
      shoesColumns[0].length <= 0?<div></div>:
      <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
        <h1 className="display-5 fw-bold">Find your shoes</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The only resource you'll ever need to find the right pair of shoes.
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add Shoes</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>Current Collection</h2>
        <div className="row">
          {shoesColumns.map((shoeList, index) => {
            return (
              <ShoesColumn key={index} list={shoeList} getShoes={fetchData} />
            );
          })}
        </div>
      </div>
    </>
    );
  }

  export default ShoesList;
