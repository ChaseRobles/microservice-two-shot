import React, { useEffect, useState } from 'react';

function ShoesForm() {
  const [bins, setBins] = useState([])



  const [formData, setFormData] = useState({
    manufacturer: '',
    name: '',
    color: '',
    picture_url: '',
    bin: ''
  })

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/shoes/';
    console.log(url)
    const fetchConfig = {
      method: "post",

      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };




    const response = await fetch(url, fetchConfig);

    if (response.ok) {

      setFormData({
        manufacturer: '',
        name: '',
        color: '',
        picture_url: '',
        bin: ''
      });
    }
  }


  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;



    setFormData({

      ...formData,


      [inputName]: value
    });
  }


//   String.prototype.nthLastIndexOf = function(searchString, n){
//     var url = this;
//     if(url === null) {
//         return -1;
//     }
//     if(!n || isNaN(n) || n <= 1){
//         return url.lastIndexOf(searchString);
//     }
//     n--;
//     return url.lastIndexOf(searchString, url.nthLastIndexOf(searchString, n) - 1);
// }


  return (
    <div className="my-5 container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add new shoe</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">

            <div className="form-floating mb-3">
              {/* <!-- Now, each field in our form references the same function --> */}
              <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture</label>
            </div>
{/*
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div> */}

            <div className="mb-3">
              <select onChange={handleFormChange} required name="bin" id="bin" className="form-select">
                <option value="">Choose a Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                    )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
export default ShoesForm;
