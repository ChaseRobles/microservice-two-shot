import { useEffect, useState } from "react";


function HatList () {
    const [hats, setHats] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        } 
    }

    const deleteIt = async(id) => {
        const del = `http://localhost:8090/api/hats/${id}/`;
        const fetchConfig = {method: 'delete',};
    
        const response = await fetch(del, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Closet Name</th>
                    <th>Section Number</th>
                    <th>Shelf Number</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.location.closet_name }</td>
                            <td>{ hat.location.section_number }</td>
                            <td>{ hat.location.shelf_number }</td>
                            <td>
                                <img src={ hat.picture_url } alt="Surprise Pikachu Face" width='100' length='100'/>
                            </td>
                            <td>
                                <button onClick={() => deleteIt(hat.id)}>Delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}
export default HatList;
