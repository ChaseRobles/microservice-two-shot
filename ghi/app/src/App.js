import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import HatList from './HatList';
import HatForm from './HatForm';




function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes/new" element={<ShoesForm />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path='/hats' element={<HatList />} />
          <Route path='new' element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
