from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from django.http import JsonResponse    


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        'closet_name',
        'section_number',
        'shelf_number',
    ]

# class HatEncoder(ModelEncoder):
#     model = Hat
#     properties = [
#         'id',
#         'style_name',
#         'picture_url',
#     ]
class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [  
        'fabric',
        'style_name',
        'color',
        'picture_url',
        'id',
        'location'
    ]
    encoders = {
        'location': LocationVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"] 
            location = LocationVO.objects.get(import_href=location_href)
            content['location'] = location
        except LocationVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Location ID"},
                    status=400,
                )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hats(request, id):
    if request.method == "GET":
        try:
            hats = Hat.objects.get(id=id)
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat Does Not Exist"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            hats = Hat.objects.get(id=id)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat Does Not Exist"}
            )
    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=id).update(**content)
        hats = Hat.objects.get(id=id)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False
        )
