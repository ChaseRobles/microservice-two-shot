from django.db import models
# from wardrobe.api.wardrobe_api.models import Bin
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

    # class Meta:
    #     ordering = ("name",)
# Create your models here.
